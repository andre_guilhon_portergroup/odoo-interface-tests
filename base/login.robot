*** Settings ***
Resource    base.robot
Suite Setup   Abrir Navegador
Suite Teardown  Close Browser

*** Test Cases ***
Login Correto
    Login Odoo      admin   123     http://localhost:8069/web/login
    Logout Odoo

Login Incorreto 
    Run Keyword And Expect Error    *   Login Odoo      admin   321     http://localhost:8069/web/login