*** Settings ***
Documentation     Base elements and keywords
Library           SeleniumLibrary

*** Keywords ***
Abrir Navegador
    Open Browser    about:blank     Chrome
    Maximize Browser Window

Fechar Navegador
    Close Browser

Login Odoo
    [Arguments]    ${login}     ${password}     ${URL}
    Go To   ${URL}
    Wait Until Element Is Visible   id=login
    Press Keys      id=login        ${login}
    Press Keys      id=password     ${password}
    Submit Form
    Wait Until Element Is Visible   class:o_user_menu

Logout Odoo
    Click Element       class:o_user_menu
    Wait Until Element Is Visible    xpath=//a[@data-menu="logout"]
    Click Link       Sair

Acessar Menu Principal
    Wait Until Element Is Visible   //a[@title='Candidaturas']
    Click Element       //a[@title='Candidaturas']

Acessar Aplicativo
    [Arguments]     ${aplicativo}
    Wait Until Element Is Visible   //div[contains(text(), '${aplicativo}')]
    Click Element       //div[contains(text(), '${aplicativo}')]
    Wait Until Page Does Not Contain Element    //div[@class='o_home_menu_scrollable']

Acessar Link
    [Arguments]     ${locator}
    Wait Until Element Is Visible   ${locator}
    Click Element   ${locator}

Acessar Menu de Aplicativo
    [Arguments]     ${menu}
    Wait Until Element Is Visible  partial link: ${menu}
    Click Link      partial link: ${menu}

Acessar SubMenu de Aplicativo
    [Arguments]     ${menu}     ${submenu}
    Wait Until Element Is Visible   partial link: ${menu}
    Click Link      partial link: ${menu}
    Wait Until Element Is Visible   partial link: ${submenu}
    Click Link      partial link: ${submenu}
    
Selecionar Linha em árvore
    [Arguments]     ${produto}
    Acessar Link  //td[contains(@class, 'o_data_cell') and contains(text(), '${produto}')]