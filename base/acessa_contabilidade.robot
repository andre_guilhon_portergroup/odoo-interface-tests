*** Settings ***
Resource    base.robot
Suite Setup   Abrir Navegador
Suite Teardown  Fechar Navegador

*** Test Cases ***
Acessar Produto da Contabilidade
    Login Odoo      Um email   Uma Senha     http://localhost:8069/web/login
    Acessar Aplicativo  Contabilidade
    Acessar SubMenu de Aplicativo  Fornecedores     Produtos
    Selecionar Linha em árvore      Mastro para fixar antena
    Sleep   5 seconds
    Acessar Menu Principal
    Logout Odoo
